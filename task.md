判断参数合法

判断list是否为空

in：list

out：boolean





商品数量信息

in：barcodes:String, Map<String,Integer> 商品数量map

out：int 商品数量



查询商品名称

in：barcode:String, Map<String, Item>商品数据库列表

out：name



查询商品价格

in：barcode，Map<String, Item>商品数据库列表

out：price：int



形成单个商品收据信息

in：barcode，Map<String, Item>商品数据库列表，Map<String,Integer> 商品数量map

out：String单个商品的收据信息



形成总商品收据信息

in：Map<String, Item>商品数据库列表，Map<String,Integer> 商品数量map

out：总商品收据信息



