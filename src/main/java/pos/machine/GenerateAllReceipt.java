package pos.machine;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class GenerateAllReceipt {
    public Map<String,Integer> getItemTpyeAndQuantity(List<String> barcodes){
        Map<String,Integer> itemTypeAndQuantity = new TreeMap<>();
        barcodes.forEach(barcode->itemTypeAndQuantity.compute(barcode,(k,v)->v==null?1:++v));
        return itemTypeAndQuantity;

    }

    public Map<String,Item> avertItemlistToMap(List<Item> itemList){
        Map<String, Item> itemMap = new TreeMap<>();
        for (Item item:itemList){
            itemMap.put(item.getBarcode(),item);
        }
        return itemMap;
    }

    public int getItemQuantity(Map<String,Integer> itemTypeAndQuantity, String barcode){
        int quantity = itemTypeAndQuantity.get(barcode);
        return quantity;
    }

    public String getItemName(String barcode,Map<String, Item> itemList){
        String name = itemList.get(barcode).getName();
        return name;
    }

    public int getSingleItemPrice(String barcode,Map<String, Item> itemList,Map<String,Integer> itemTypeAndQuantity){
        int price = itemList.get(barcode).getPrice();
        int quantity = getItemQuantity(itemTypeAndQuantity,barcode);
        int singlePrice=price*quantity;
        return singlePrice;
    }

    public int getItemTotalPrice(Map<String,Integer> itemTypeAndQuantity,Map<String, Item> itemList){
        int totalPrice=0;

        for (Map.Entry<String,Integer> item:itemTypeAndQuantity.entrySet()
        ) {
            int price = getSingleItemPrice(item.getKey(),itemList,itemTypeAndQuantity);
            totalPrice = totalPrice+price;
        }
        return totalPrice;
    }

    public String generateSingleItemReceipt(String barcode,Map<String,Integer> itemTypeAndQuantity,Map<String, Item> itemList){
        String name = getItemName(barcode,itemList);
        int quantity = getItemQuantity(itemTypeAndQuantity,barcode);
        int price = itemList.get(barcode).getPrice();
        int subtotalPrice = getSingleItemPrice(barcode,itemList,itemTypeAndQuantity);
        String singleItemReceipt="Name: "+name+", Quantity: "+quantity+", Unit price: "+price+" (yuan), Subtotal: "+subtotalPrice+" (yuan)\n";
        return singleItemReceipt;
    }

    public String generateReceipt(Map<String, Integer> itemTypeAndQuantity, Map<String, Item> itemList){
        String receipt="***<store earning no money>Receipt***\n";
        for (Map.Entry<String,Integer> barcode:itemTypeAndQuantity.entrySet()
        ) {
            receipt = receipt+generateSingleItemReceipt(barcode.getKey(), itemTypeAndQuantity,itemList);

        }
        receipt=receipt+"----------------------\nTotal: " +getItemTotalPrice(itemTypeAndQuantity,itemList)+" (yuan)\n**********************";

        return receipt;
    }
}
