package pos.machine;

import java.util.List;
import java.util.Map;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        IfParameterValid ifParameterValid = new IfParameterValid();
        if (ifParameterValid.ifListIsNull(barcodes)){
            return null;
        }
        else {
            ItemsLoader itemsLoader = new ItemsLoader();
            GenerateAllReceipt generateAllReceipt = new GenerateAllReceipt();
            List<Item> itemList = itemsLoader.loadAllItems();
            Map<String, Item> stringItemMap = generateAllReceipt.avertItemlistToMap(itemList);

            Map<String, Integer> itemTpyeAndQuantity = generateAllReceipt.getItemTpyeAndQuantity(barcodes);
            String receipt = generateAllReceipt.generateReceipt(itemTpyeAndQuantity, stringItemMap);
            return receipt;
        }

    }

}
